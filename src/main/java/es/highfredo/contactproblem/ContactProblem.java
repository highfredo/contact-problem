package es.highfredo.contactproblem;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

public class ContactProblem implements IContactProblem {
	
	private int minLength, maxLength;
	private String message;
	private Map<String, Integer> frecuencyMap;

	@Override
	public void setData(int minLength, int maxLength, String message) {
		if(minLength < 1 || maxLength < 1 || message == null) throw new IllegalArgumentException();
		
		this.minLength = minLength;
		this.maxLength = maxLength;
		this.message = message;
		this.frecuencyMap = new HashMap<>();
	}

	@Override
	public void run() {
		for(int currentLength = minLength; currentLength <= maxLength; currentLength++) {
			int index = 0;
			while(index + currentLength <= message.length()) {
				String currentPart = message.substring(index, index+currentLength);
				Integer currentFrecuency = frecuencyMap.getOrDefault(currentPart, 0);
				frecuencyMap.put(currentPart, ++currentFrecuency);
				index++;
			}
		}
	}

	@Override
	public Map<String, Integer> getResult(int topRank) {
		if(topRank < 1) throw new IllegalArgumentException();

		Map<String, Integer> result = new LinkedHashMap<>();		
		frecuencyMap.entrySet().stream()
		    .sorted((e1, e2) -> e2.getValue().compareTo(e1.getValue())).limit(topRank)
	    		.forEach(e -> result.put(e.getKey(), e.getValue()));

	    return result;
	}
	
	
}
